//
//  CellPullRequest.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class CellPullRequest: UITableViewCell, NibReusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUpdatedAt: UILabel!
    
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    static func height () -> CGFloat {
        return 180
    }
    
    func setup(with pull: PullRequest) {
        
        lblTitle.text = "\(pull.title)"
        lblDescription.text = "\(pull.body)"
        
        if let date = pull.updatedAt {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            
            lblUpdatedAt.text = formatter.string(from: date)
        } else {
            lblUpdatedAt.text = ""
        }
        
        if let owner = pull.owner {
            lblName.text = "\(owner.login)"
            imgOwner.layer.cornerRadius = 28
            imgOwner.layer.masksToBounds = true
            imgOwner.kf.setImage(with: URL(string: owner.avatar))
        } else {
            lblName.text = ""
        }
        
    }
}
