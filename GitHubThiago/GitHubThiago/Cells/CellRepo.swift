//
//  CellRepo.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class CellRepo: UITableViewCell, NibReusable {

    @IBOutlet weak var lblRepoName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    
    static func height () -> CGFloat {
        return 160
    }
    
    func setup(with repos: Repos) {
        
        lblRepoName.text = repos.name
        lblDescription.text = repos.descriptionRepo
        
        lblForks.text = "\(repos.forks)"
        lblStar.text = "\(repos.star)"
        
        if let owner = repos.owner {
            lblOwnerName.text = owner.login
            
            imgOwner.layer.cornerRadius = 40
            imgOwner.layer.masksToBounds = true
            imgOwner.kf.setImage(with: URL(string: owner.avatar))
        } else {
            lblOwnerName.text = ""
        }
        
    }
}
