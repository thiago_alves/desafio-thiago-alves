//
//  JavaRepoVC.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON

class JavaRepoVC: UIViewController {

    @IBOutlet weak var table: UITableView!

    var tableDatasource: RepoTableDatasource?
    var tableDelegate: RepoTableDelegate?

    var repos: [Repos] = []

    // Pagination
    var indexOfPageToRequest = 1
    var isLoadingFromServer = false
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.tintColor = .white
        requestRepos()
    }

    // MARK: - Fileprivate Methods
    fileprivate func requestRepos() {

        isLoadingFromServer = true
        
        APIManager.request(.repos(pages: indexOfPageToRequest)).responseJSON { (response) in

            if let result = response.result.value as? Dictionary<String, Any> {

                let repoList = Repos.parseList(JSON(result), arrayName: "items")

                self.isLoadingFromServer = false
                
                self.repos += repoList
                self.tableDelegate = RepoTableDelegate(self)
                self.tableDatasource = RepoTableDatasource(items: self.repos, tableView: self.table, delegate: self.tableDelegate!)
            }
        }

    }
}

// MARK: - TableDelegate
extension JavaRepoVC: TableDelegate {
    func didSelect(at index: IndexPath) {
        
        let pullVC: PullRequestsVC = UIStoryboard(storyboard: .main).instantiateViewController()
        pullVC.repo = self.repos[index.row]
        navigationController?.pushViewController(pullVC, animated: true)
    }
    
    func incrementPage() {
        
        if isLoadingFromServer {
            return
        }
        
        indexOfPageToRequest += 1
        requestRepos()
    }
}
