//
//  PullRequestsVC.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequestsVC: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    var repo: Repos?
    
    var tableDatasource: PullDatasource?
    var tableDelegate: PullTableDelegate?
    
    var pullList: [PullRequest] = []
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = repo?.name
        requestPull()
    }

    // MARK: - Fileprivate Methods
    fileprivate func requestPull () {
        
        guard let repo = repo else {
            return
        }
        
        APIManager.request(.pullRequests(repo: repo.fullName)).responseJSON { (response) in
        
            if let result = response.result.value {
                                
                let pullList = PullRequest.parseList(JSON(result))
                
                self.pullList = pullList
                self.tableDelegate = PullTableDelegate(self)
                self.tableDatasource = PullDatasource(items: self.pullList, tableView: self.table, delegate: self.tableDelegate!)
            }
        }
    }

}

// MARK: - TableDelegate
extension PullRequestsVC: TableDelegate {
    func didSelect(at index: IndexPath) {
        
        let webVC: WebViewVC = UIStoryboard(storyboard: .web).instantiateViewController()
        webVC.urlString = pullList[index.row].html
        navigationController?.pushViewController(webVC, animated: true)
    }
    
    func incrementPage() {

    }
}

