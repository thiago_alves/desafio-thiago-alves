//
//  WebViewVC.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//
import UIKit
import WebKit

class WebViewVC: UIViewController {
    
    @IBOutlet weak var viewContainer: UIView!
    
    var wkWebView = WKWebView()
    var urlString: String?
    var navTitle = ""
    
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        wkWebView = WKWebView.init(frame: viewContainer.frame)
        
        if urlString != nil {
            if let url = URL(string: urlString!) {
                let request = URLRequest(url: url)
                wkWebView.load(request)
                wkWebView.navigationDelegate = self
                view.addSubview(wkWebView)
            }
        }
    }
}

// MARK: - WKWebView
extension WebViewVC: WKNavigationDelegate, WKUIDelegate {
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let mainFrame = navigationAction.targetFrame?.isMainFrame {
            if !mainFrame {
                webView.load(navigationAction.request)
            }
        }
        
        return nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if webView != wkWebView {
            decisionHandler(.allow)
            return
        }
        
        let app = UIApplication.shared
        if let url = navigationAction.request.url {
            // Handle target="_blank"
            if navigationAction.targetFrame == nil {
                if app.canOpenURL(url) {
                    app.open(url, options: [:], completionHandler: nil)
                    decisionHandler(.cancel)
                    return
                }
            }
            
            // Handle phone and email links
            if url.scheme == "tel" || url.scheme == "mailto" {
                if app.canOpenURL(url) {
                    app.open(url, options: [:], completionHandler: nil)
                    decisionHandler(.cancel)
                    return
                }
            }
            
            decisionHandler(.allow)
        }
    }
    
}
