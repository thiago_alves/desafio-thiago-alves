//
//  RepoDatasource.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit

class RepoTableDatasource: NSObject, UITableViewDataSource {

    var items: [Repos] = []
    weak var tableView: UITableView?
    weak var delegate: UITableViewDelegate?
    
    required init(items: [Repos], tableView: UITableView, delegate: UITableViewDelegate) {
        self.items = items
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: CellRepo.self)
        self.setupTableView()
    }
    
    func setupTableView() {
        self.tableView?.dataSource = self
        self.tableView?.delegate = self.delegate
        self.tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CellRepo.self)
        cell.setup(with: items[indexPath.row])
        return cell
    }
}

class RepoTableDelegate: NSObject, UITableViewDelegate {
    
    let delegate: TableDelegate
    
    init(_ delegate: TableDelegate) {
        self.delegate = delegate
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CellRepo.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didSelect(at: indexPath)
    }
}

extension RepoTableDelegate: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        if offsetY <= 0 {
            return
        }
        
        let contentHeight = scrollView.contentSize.height
        if (offsetY + 350.0) > (contentHeight - scrollView.frame.size.height) {
            delegate.incrementPage()
        }
    }
}
