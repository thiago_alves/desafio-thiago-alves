//
//  APIManager.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import Alamofire

public protocol TargetType {
    
    /// The target's base `URL`.
    var baseURL: String { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: Alamofire.HTTPMethod { get }
    
    /// The parameters to be incoded in the request.
    var parameters: [String: Any]? { get }
}

enum GitHub {
    
    // MARK: Main
    case repos(pages: Int)
    case pullRequests(repo: String)
}


extension GitHub: TargetType {
    
    /// The target's base `URL`.
    var baseURL: String { return "https://api.github.com/" }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .repos(pages: let page):
            return "search/repositories?q=language:Java&sort=stars&page=\(page)"
        case .pullRequests(repo: let repo):
            return "repos/\(repo)/pulls"
        }
    }
    
    /// The HTTP method used in the request.
    var method: Alamofire.HTTPMethod {
        switch self {
        case .repos, .pullRequests:
            return .get

        }
    }
    
    /// The parameters to be incoded in the request.
    var parameters: [String: Any]? {
        switch self {
        case .repos, .pullRequests:
            return [:]
        }
    }
    
    /// The method used for parameter encoding.
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .repos, .pullRequests:
            return URLEncoding.default
        }
    }
}

final class APIManager {
    
    // MARK: - Request creation with endpoint
    
    static func request(_ endpoint: GitHub) -> DataRequest {
//        var headers: HTTPHeaders?
        return Alamofire.request(endpoint.baseURL + endpoint.path,
                                 method: endpoint.method,
                                 parameters: endpoint.parameters,
                                 encoding: endpoint.parameterEncoding,
                                 headers: nil)
    }
}

extension Dictionary {
    
    static func += <K, V> ( left: inout [K:V], right: [K:V]) {
        for (k, v) in right {
            left.updateValue(v, forKey: k)
        }
    }
}
