//
//  Repos.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON

class Repos: NSObject {
    
    var id = 0
    var name = ""
    var fullName = ""
    var forks = 0
    var descriptionRepo = ""
    var star = 0
    
    var owner: Owner?
    
    // MARK: - Parse
    static func parseList(_ json: JSON, arrayName: String) -> [Repos] {
        var array = [Repos]()
        
        for oneElem in json[arrayName].arrayValue {
            let repo = Repos.parseOne(oneElem)
            array.append(repo)
        }
        
        return array
    }
    
    static func parseOne(_ json: JSON) -> Repos {
        let oneRepo = Repos()

        oneRepo.id = json["id"].intValue
        oneRepo.name = json["name"].stringValue
        oneRepo.fullName = json["full_name"].stringValue
        oneRepo.forks = json["forks_count"].intValue
        oneRepo.descriptionRepo = json["description"].stringValue
        oneRepo.star = json["stargazers_count"].intValue
        
        oneRepo.owner = Owner.parseOne(json["owner"])
        
        return oneRepo
    }
}

