//
//  Owner.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON

class Owner: NSObject {

    var id = 0
    var login = ""
    var avatar = ""
    
    static func parseOne(_ json: JSON) -> Owner {
        let owner = Owner()
        
        owner.id = json["id"].intValue
        owner.avatar = json["avatar_url"].stringValue
        owner.login = json["login"].stringValue
        
        return owner
    }
}
