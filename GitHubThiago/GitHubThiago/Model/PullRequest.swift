//
//  PullRequest.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequest: NSObject {

    var title = ""
    var body = ""
    var html = ""
    
    var updatedAt: Date?
    
    var owner: Owner?
    
    // MARK: - Parse
    static func parseList(_ json: JSON) -> [PullRequest] {
        var array = [PullRequest]()
        
        for oneElem in json.arrayValue {
            let pull = PullRequest.parseOne(oneElem)
            array.append(pull)
        }
        
        return array
    }
    
    static func parseOne(_ json: JSON) -> PullRequest {
        let pull = PullRequest()
        
        pull.html = json["html_url"].stringValue
        pull.title = json["title"].stringValue
        pull.body = json["body"].stringValue
        pull.owner = Owner.parseOne(json["user"])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
        dateFormatter.timeZone = TimeZone(abbreviation: localTimeZoneAbbreviation) //Current time zone
        let date = dateFormatter.date(from: json["updated_at"].stringValue)
        pull.updatedAt = date
        
        return pull
    }
}
